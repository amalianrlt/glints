import React from "react";
import "../assets/style/MainContent.css"

const BookCard = props => {
  const book = props.books.map(item =>(
    <div className="book-card" key={item.id}>
      <img className="img-book" src= { item.image } alt={ item.image } />
      <h4 className ="title-book">{item.title}</h4>
      <p className ="desc-book" >{item.description}</p>
    </div>
)
)
  return(
    <div className="book-container">
      { book }
    </div>
  )
}

export default BookCard;
