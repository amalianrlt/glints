import React, { Component } from "react";
import BookCard from './BookCard';
import "../assets/style/MainContent.css"

class BookList extends Component{
  state = {
    books:[
      {
        id : 1,
        title : "Macroeconomics 7th Edition",
        image : require ("../assets/image/book1.jpg"),
        description : " It maintains that bestselling status by continually bringing the leading edge of macroeconomics theory, research, and policy to the classroom, explaining complex concepts with exceptional clarity. ",
        author : "N. Gregory Mankiw"
      },
      {
        id : 2,
        title : "Cambridge IGCSE Economics",
        image : require ("../assets/image/book5.jpg"),
        description : "The book draws extensively on real world examples to explore economic concepts, theories and issues. Each of the 52 units deals with a specific topic in a lucid and pertinent manner.",
        author : "Susan Grant"
      },
      {
       id : 3,
       title : "Monetary Policy and Central Banking: New Directions in Post-Keynesian Theory",
       image : require ("../assets/image/book3.jpg"),
       description : "This book presents a detailed, multi-faceted analysis of banking and monetary policy. The first part examines the role of central banks within an endogenous money framework. These chapters address post-Keynesian interest rate policy, monetary mercantilism, financial market organization and developing economies.",
       author : "Louis-Philippe Rochon,"
      }    
    ]
  }
  
  render(){
    return(
      <div>
        <BookCard className="books"books={ this.state.books }/>
      </div>
    )
  }
}

export default BookList;
