import React from "react";

const MainTitle = () => { 
  return (
    <div>
      <h1 style = {{ textAlign: "center" }} >My Favorite Books </h1>
    </div>
  )
}

export default MainTitle;
