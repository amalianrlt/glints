import React from 'react';
import MainTitle from './components/MainTitle.js'
import BookList from './components/BookList.js'
import './assets/style/MainContent.css'


class App extends React.Component{
  render(){
    return(
      <div>
        <MainTitle/>
        <BookList/>
     </div>
    )
  }
}

export default App;
